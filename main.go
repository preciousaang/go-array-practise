package main

import "fmt"

type floatMap map[string]float64

func (m floatMap) output() {
	fmt.Println(m)
}

func main() {
	userNames := make([]string, 2)

	userNames[0], userNames[1] = "Hullo", "World"

	userNames = append(userNames, "Max")
	userNames = append(userNames, "Ruger")

	fmt.Println(userNames)

	courseRatings := make(floatMap, 3)

	courseRatings["go"] = 2.3
	courseRatings["react"] = 4.4
	courseRatings["angular"] = 4.5
	courseRatings["python"] = 3.3

	courseRatings.output()

	for _, value := range userNames {
		fmt.Println(value)
	}

	for i, value := range courseRatings {
		fmt.Println(i, " ", value)
	}

}
