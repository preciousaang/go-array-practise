package lists

import "fmt"

func main() {
	prices := []float64{2, 4}

	fmt.Println(prices)
	fmt.Println(cap(prices))

	updatedPrices := append(prices, 40)
	fmt.Println(updatedPrices)

	discountedPrices := []float64{3, 4, 5, 2, 4}

	updatedPrices = append(updatedPrices, discountedPrices...)

	fmt.Println(updatedPrices)
}

// func main() {
// 	var products [6]string
// 	prices := [4]float64{22.22, 12.3, 11, 22.3}

// 	products = [6]string{"Book", "Pencil", "Pen", "Eraser", "Board", "Duster"}

// 	products[5] = "Table"
// 	fmt.Println(products[5])

// 	featuredPrices := prices[:4]

// 	highlightedPrices := featuredPrices[1:2]

// 	featuredPrices[0] = 44

// 	fmt.Println(len(prices))
// 	fmt.Println(len(featuredPrices), cap(featuredPrices))
// 	fmt.Println(len(highlightedPrices), cap(highlightedPrices))
// }
